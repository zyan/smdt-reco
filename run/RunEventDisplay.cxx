#include <iostream>

#include "TString.h"
#include "TTree.h"
#include "TFile.h"

#include "AtlasStyle/AtlasStyle.h"
#include "AtlasStyle/AtlasStyle.C"

#include "MuonReco/EventDisplay.h"
#include "MuonReco/Geometry.h"
#include "MuonReco/Event.h"
#include "MuonReco/ArgParser.h"
#include "MuonReco/ConfigParser.h"

using namespace MuonReco;

//int RunEventDisplay() {
int main(int argc, char* argv[]) {

  SetAtlasStyle();

  ArgParser ap = ArgParser(argc, argv);

  if (ap.hasKey("--help")) {
    std::cout << "Usage: " << std::endl 
	      << "runEventDisplay --conf CONFIG" << std::endl
	      << "                [--runN RUNNUMBER]" << std::endl
	      << "Note:" << std::endl
              << "RUNUMBER will override the definition" << std::endl
              << "in the config file.  " << std::endl;
  }

  ConfigParser cp = ConfigParser(ap.getTString("--conf"));

  int runN;
  if (ap.hasKey("--runN")) {
    runN = ap.getInt("--runN");
  }
  else {
    runN = cp.items("General").getInt("RunNumber");
  }
  
  // open input file
  TFile *inputFile = new TFile(IOUtility::getDecodedOutputFilePath(runN), "r");
  
  TTree *eTree = (TTree*)inputFile->Get("eTree");
  Event *e = new Event();
  eTree->SetBranchAddress("event", &e);

  std::cout << "ERH" << std::endl;
  std::cout << eTree->GetEntries() << std::endl;
  
  // set the geometry
  Geometry geo = Geometry();
  geo.SetRunN(runN);
  geo.Configure(cp.items("Geometry"));
  
  // initialize the event display
  EventDisplay ed = EventDisplay();

  bool has_perp = false;
  for (int i = 0; i < geo.orientation().size(); i++) {
    if (geo.orientation().at(i) == 1) {
      has_perp = true;
    }
  }
  if (has_perp) {
    ed.Divide(2, 1);
  }

  // while user has not exited, keep showing next event
  TString user_input = "c";
  unsigned long counter = 0;
  char buffer[256];
  int  bufflen = 256;

  std::cout << "2" << std::endl;

  while (user_input.CompareTo(TString("q")) && counter < eTree->GetEntries()) {
    eTree->GetEntry(counter);
    std::cout << "HERE" << std::endl;
    e->setID(counter);
    ed.DrawEvent(*e, geo, NULL);

    // get user input
    std::cout << "To continue, enter any key." 
	      << "To exit,     enter q" << std::endl;
    std::cin.getline(buffer, bufflen);
    user_input = TString(buffer);
    ed.Clear();
    counter++;
  }
  std::cout << "done" << std::endl;
  return 0;
}
